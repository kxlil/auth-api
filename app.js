const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
require('dotenv').config()
const InitiateMongoServer = require('./config/db')
const users = require('./routes/users')
const auth = require('./routes/auth')
const errorMiddleware = require('./middleware/error')

// Init MongoServer
InitiateMongoServer()

const app = express()

// Port
const port = process.env.PORT || 4000

// Middleware
app.use(express.json())
app.use(cors())

app.get('/', (req, res) => {
  res.send('Welcom to Auth_API')
})

app.use('/auth', auth)
app.use('/users', users)

app.use(errorMiddleware)

const server = app.listen(port, () => {
  console.log(`Auth_API app listening at http://localhost:${port}`)
})

// Graceful shutdown
function handleGracefullShutdown (sig) {
  console.info(`${sig} signal received.`)
  console.log('Closing http server...')
  server.close(() => {
    console.log('Http server closed.')
    mongoose.connection.close(false, () => {
      console.log('MongoDb connection closed.')
      process.exit(0)
    })
  })
}

process.on('SIGINT', handleGracefullShutdown)
process.on('SIGTERM', handleGracefullShutdown)
process.on('SIGHUP', handleGracefullShutdown)
