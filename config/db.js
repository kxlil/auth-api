const mongoose = require('mongoose')
const mongouri = `${process.env.MONGO_URL}`
// const mongouri = 'mongodb://test:test@localhost/test'

const InitiateMongoServer = async () => {
  try {
    await mongoose.connect(mongouri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false
    })
    console.log('DB Connected Successfully')
  } catch (err) {
    console.log(err)
    throw err
  }
}

module.exports = InitiateMongoServer
