const handleDuplicateKeyError = (err, res) => {
  const field = Object.keys(err.keyValue)
  const code = 409
  res.status(code).json({
    message: `This ${field} is already used`
  })
}

const handleValidationError = (err, res) => {
  const errors = Object.values(err.errors).map(el => { return { message: el.message } })
  const error = errors[0]
  const code = 400
  res.status(code).json({ error })
}

module.exports = (err, req, res, next) => {
  try {
    if (err.name === 'ValidationError') return handleValidationError(err, res)
    if (err.code && err.code === 11000) return handleDuplicateKeyError(err, res)
    throw err
  } catch (err) {
    res.status(500).send({ message: 'An unknown error occurred' })
  }
}
