const express = require('express')
const tokenValidator = require('../middleware/auth')
const router = express.Router()

const User = require('../model/User')

router.use(tokenValidator)

router.get('/me', async (req, res, next) => {
  try {
    const user = await User.findById(req.user.id, { email: 1, username: 1, admin: 1 })
    res.status(200).json({
      user
    })
  } catch (err) {
    next(err)
  }
})

router.put('/me', async (req, res, next) => {
  const {
    email,
    password
  } = req.body

  const update = { email, password }

  // remove undefined properties
  for (const key in update) {
    if (update[key] === undefined) {
      delete update[key]
    }
  }

  try {
    await User.updateOne({ _id: req.user.id }, update)

    res.sendStatus(204)
  } catch (err) {
    next(err)
  }
})

module.exports = router
