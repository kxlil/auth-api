const express = require('express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const router = express.Router()

const User = require('../model/User')

router.post('/signup', async (req, res, next) => {
  const {
    username,
    email,
    password
  } = req.body

  try {
    const user = new User({
      username,
      email,
      password
    })

    await user.save()

    res.sendStatus(201)
  } catch (err) {
    next(err)
  }
})

router.post('/signin', async (req, res) => {
  const {
    email,
    password
  } = req.body

  try {
    const user = await User.findOne({
      email
    })

    if (!user) {
      return res.status(401).json({
        message: 'Email or password is incorrect'
      })
    }

    const match = await bcrypt.compare(password, user.password)
    if (!match) {
      return res.status(401).json({
        message: 'Email or password is incorrect'
      })
    }

    const payload = {
      user: {
        id: user.id
      }
    }

    jwt.sign(
      payload,
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: 1200 },
      (err, token) => {
        if (err) throw err
        res.status(200).json({
          token
        })
      }
    )
  } catch (err) {
    res.status(500).send(err)
  }
})

module.exports = router
